import os
from rhyme import Rhyme
from reader import Reader
import spacy
import joblib
import os

nlp = spacy.load("en_core_web_sm")

positive = []
with open('model/sentiment/opinion-lexicon-english/positive-words.txt', 'r', encoding='ISO-8859-1') as f:
  lines = f.readlines()
for line in lines:
  if ';' in line or line == '\n':
    continue
  positive.append(line.strip('\n'))

negative = []
with open('model/sentiment/opinion-lexicon-english/negative-words.txt', 'r', encoding='ISO-8859-1') as f:
  lines = f.readlines()
for line in lines:
  if ';' in line or line == '\n':
    continue
  negative.append(line.strip('\n'))

def count_sentiment(s):
  pos_count = 0
  neg_count = 0
  docs = nlp(s)
  for token in docs:
    if token.lemma_ in positive:
      pos_count = pos_count + 1
    if token.lemma_ in negative:
      neg_count = neg_count + 1
  return pos_count, neg_count

def list_2_str(poem):
  return ' '.join(poem)

def sentiment_score(pos, neg):
  if pos+neg == 0:
    return 0
  return (pos-neg)/(pos+neg)

def sentiment_label(score):
  if score > 0:
    return 'positive'
  elif score < 0:
    return 'negative'
  else:
    return 'neutral'

src = "dataset/poetry_txt/1875-1900_Poetry.txt"

model_rhyme = Rhyme()
model_rhyme.load_model()
counter = 1

reader = Reader(src)
dest = src.replace("poetry_txt", "poetry_labeled")

# Read src file line by line
with open(src, mode="r", encoding="utf-8") as src_file:
  content = src_file.readlines()
# Open dest file
dest_file = open(dest, mode="a", encoding="utf-8")
stanza = list()
header = ""
footer = ""
time_epoch = ""
sentiment = ""
tracker = 0

# Define time epoch based on name of txt file
if ("1600" in src and "1700" in src) or ("1500" in src and "1600" in src):
  time_epoch = "0"
elif "1700" in src and "1800" in src:
  time_epoch = "1"
elif "1800" in src and "1850" in src:
  time_epoch = "2"
elif ("1850" in src and "1875" in src) or ("1875" in src and "1900" in src) or "2000" in src:
  time_epoch = "3"

# Write file
for line in content:
  is_footer = False
  if ("<|startoftext|>" in line):
    header = line
    stanza = list()
    continue
  if ("<|endoftext|>" in line):
    footer = line
    is_footer = True
  if not is_footer:
    stanza.append(line)
  else:
    rhyme = model_rhyme.predict_seq(reader.get_ending_words(stanza))
    sentiment = sentiment_label(sentiment_score(count_sentiment(list_2_str(stanza))[0],count_sentiment(list_2_str(stanza))[1]))
    header = header.replace("SENTIMENT_LABEL", sentiment)
    header = header.replace("RHYME_SCHEME", rhyme)
    header = header.replace("TIME_EPOCH", time_epoch)

    dest_file.write(header)
    tracker +=1
    print(tracker)
    for line in stanza:
      dest_file.write(line)
      tracker +=1
      print(tracker)
    dest_file.write(footer)
    tracker +=1
    print(tracker)

src_file.close()
dest_file.close()

counter += 1