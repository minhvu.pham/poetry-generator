import os

path = "dataset/poetry_4-10"

counter = 1

for (dirpath, dirnames, filenames) in os.walk(path):
  for filename in filenames:
    src = os.path.join(dirpath, filename)
    dest = path + ".txt"
    # Debug
    print("File %d at: %s" % (counter, src))

    src_file = open(src, 'r', encoding='utf8')
    dest_file = open(dest, 'a', encoding='utf8')

    lines = src_file.readlines()
    for line in lines:
      dest_file.write(line)

    src_file.close()
dest_file.close()

    counter += 1
