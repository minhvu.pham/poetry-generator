from flask import Flask, request, render_template
from gpt2.interactive_conditional_samples import interact_model
import logging
from datetime import datetime
from pytz import timezone
app = Flask(__name__)

logging.basicConfig(filename='error.log', level=logging.ERROR)

german_tz = timezone('Europe/Berlin')
start_time = datetime.now(german_tz)


@app.route('/')
def index():
    global start_time
    start_time = datetime.now(german_tz)

    return render_template('index.html')


@app.route('/', methods=['GET', 'POST'])
def generate():
    f = open("log.txt", "a")

    rhyme = str(request.form['rhyme'])
    sent = str(request.form['sentiment'])
    time = str(request.form['time-epoch'])

    seed = "<|startoftext|> [" + rhyme + ", " + sent + ", " + time + ", 4]"
    poem_txt = interact_model(input=seed, length=len(rhyme)*45)
    poem_txt = poem_txt[:poem_txt.find("<|endoftext|>")]
    poem = poem_txt.replace("\n", "<br>")
    time = time + " century"

    f.write("Start: " + str(start_time) + "\n")
    end_time = datetime.now(german_tz)
    f.write("End: " + str(end_time) + "\n")
    duration = end_time - start_time
    f.write("Duration: " + str(duration) + "\n\n")

    f.write(rhyme.upper() + ", " + sent.upper() + ", " + time + "\n")
    f.write(poem_txt + "\n")
    f.write("-"*100 + "\n")
    f.close()

    return render_template('index.html', rhyme=rhyme, sent=sent, time=time, output=poem, display=True)


if __name__ == "__main__":
    app.run()
