# Poetry Generator

## Run Web application
Navigate to webapp:

    cd webapp
Setup a Python virtual environment (optional):
``` 
virtualenv --system-site-packages -p python3 env
source env/bin/activate
```

Install the requirements:
```
env/bin/pip3 install -r requirements.txt
```
Run the web application:

    python app.py

## Label the dataset used to fine-tune GPT-2
Convert from xml to txt:

    python label_convert_to_txt.py

Label the dataset with rhyme, sentiment and time epoch:

    python label.py
## Fine-tune GPT-2 model
First we need to convert the dataset to .npz file:
Navigate to webapp/gpt2/

    cd webapp/gpt2
Create npz data:

    python encode.py <input-file> <output-file>
 Download GPT-2 original model:

    python download_model.py 345M

Start fine-tuning GPT-2:

    python train.py --save-every 2000 --sample-every 2000
The trained model is saved inside

    webapp/gpt2/models/checkpoint
    

## UKP model
Slight modification:
- Save_and_load.py: only for loading trained model (for further training)

    command: `python3 Save_and_Load.py [model_name]`
    
- RunModel_CoNLL_Format.py: add third parameter to specify the directory of the output file

    command: `python3 RunModel_CoNLL_Format.py [model_name] [conll_file] [stored_location]`
