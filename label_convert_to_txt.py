import os
from reader import Reader

path = "dataset/poetry/1900-2000_Poetry"

counter = 1

for (dirpath, dirnames, filenames) in os.walk(path):
  for filename in filenames:
    src = os.path.join(dirpath, filename)

    # Debug
    print("File %d at: %s" % (counter, src))

    dest = dirpath.replace("/poetry", "/poetry_txt") + ".txt"

    try:
      reader = Reader(src)        
      reader.get_poem()
      reader.convert_to_txt(src, dest)
    except:
      print("Error: Skip this file")
      continue

    counter += 1
