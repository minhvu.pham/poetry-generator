import string
import math
from rhyme import Rhyme
from senex import SentimentExtractor
from epoch import Epoch

class Test:
    se = SentimentExtractor()
    ti = Epoch()
    model_rhyme = Rhyme()
    model_rhyme.load_model()
    #(TYPE,TOTAL)
    rtypes = []
    stypes = []
    ttypes = []

    correct = [0, 0, 0]  # (RHYME,SENTIMENT,TIMEEPOCH)
    rtotal = 0
    stotal = 0
    ttotal = 0
    total = 0

    def count(self,path):
        lines = []
        process = False
        with open(path, 'r', encoding='utf8') as f:
            data = f.readlines()
            param = ""
            for l in data:
                if "<|s" in l:
                    lines = []
                    param = l
                    process = True
                elif "<|e" in l and process:
                    print(lines)
                    self.total = self.total + 1
                    self.check(lines,param)
                    lines = []
                else:
                    l = l.strip()
                    if len(l) != 0 and l[len(l) - 1] in string.punctuation:
                        l = l.strip(l[len(l) - 1])      # strip punctuation at the end of line
                    lines.append(l)
        self.printresult()

    def check(self,lines,param):
        #<|startoftext|> [abab, negative, 19th, 4] ~ input parameter
        split = param[17:len(param)-1].split(", ") #abab, negative, 19th, 4
        r = split[0]
        s = split[1]
        t = split[2]

        #extractors
        #return scheme i.e "aabb"
        def rhycheck():
            end_words = []
            for line in lines:
                split = line.split()
                end = split[len(split)-1].lower()
                end_words.append(end)
                scheme = self.model_rhyme.predict_seq(end_words)
            return self.equivalenceof(scheme)

        #return "neutral"/"positive"/"negative"
        def sencheck():
            sentence = self.se.list_2_str(lines)
            p, n = self.se.count_sentiment(sentence)
            return self.se.sentiment_label(self.se.sentiment_score(p, n))

        #return epoch interval
        def timcheck():
            result = self.ti.predict(lines)
            print(lines)
            if result[0] == 1: #[1,0,0,0]: 1500-1700 ~ 16 17
                return ["16th","17th"]
            if result[1] == 1: #[0,1,0,0]: 1700-1800 ~ 18
                return ["18th"]
            if result[2] == 1: #[0,0,1,0]: 1800-1850 ~ 19
                return ["19th"]
            if result[3] == 1: #[0,0,0,1]: 1850-2000 ~ 19 20
                return ["19th","20th"]
            return "ERROR"
        try:
            rhy = rhycheck()
        except:
            rhy = "ERROR"
        try:
            sen = sencheck()
        except:
            sen = "ERROR"
        try:
            tim = timcheck()
        except:
            tim = "ERROR"
        #RHYME
        if not rhy == "ERROR":
            self.rtotal = self.rtotal + 1
            exist = False
            for tup in self.rtypes:
                if tup[0] == r:
                    exist = True
                    tup[1] = tup[1] + 1
                    break
            if not exist:
                tuple = [r, 1]
                self.rtypes.append(tuple)
            if r in rhy:
                self.correct[0] = self.correct[0] + 1
        # SENTIMENT
        if not sen == "ERROR":
            self.stotal = self.stotal + 1
            exist = False
            for tup in self.stypes:
                if tup[0] == s:
                    exist = True
                    tup[1] = tup[1] + 1
                    break
            if not exist:
                tuple = [s, 1]
                self.stypes.append(tuple)
            if s == sen:
                self.correct[1] = self.correct[1] + 1
        # TIMEEPOCH
        if not tim == "ERROR":
            self.ttotal = self.ttotal + 1
            exist = False
            for tup in self.ttypes:
                if tup[0] == t:
                    exist = True
                    tup[1] = tup[1] + 1
                    break
            if not exist:
                tuple = [t, 1]
                self.ttypes.append(tuple)
            if t in tim: # check if in epoch interval
                self.correct[2] = self.correct[2] + 1

    def printresult(self):
        print("Rhyme:"+"\tSamples")
        for tup in self.rtypes:
            print(tup[0]+"\t"+str(tup[1]))
        accuracy = self.correct[0]*100/self.rtotal
        print("ACCURACY: "+str(accuracy) + "%")
        print("TOTAL: "+str(self.rtotal))
        print("ERROR: " + str(self.total - self.rtotal))

        print("-"*10)

        print("Sentiment:" + "\tSamples")
        for tup in self.stypes:
            print(tup[0]+"\t"+str(tup[1]))
        accuracy = self.correct[1]*100/self.stotal
        print("ACCURACY: " + str(accuracy) + "%")
        print("TOTAL: "+str(self.stotal))
        print("ERROR: " + str(self.total - self.stotal))

        print("-"*10)

        print("TimeEpoch:" + "\tSamples")
        for tup in self.ttypes:
            print(tup[0]+"\t"+str(tup[1]))
        accuracy = self.correct[2]*100/self.ttotal
        print("ACCURACY: " + str(accuracy)+"%")
        print("TOTAL: "+str(self.ttotal))
        print("ERROR: " + str(self.total - self.ttotal))
        return

    # abc bac bca
    # bacc == abcc == cabb == wxyy
    def equivalenceof(s):
        vocab = ['w', 'x', 'y', 'z']  # ['a', 'b', 'c', 'd']
        masks = []
        instring = []
        for char in s:
            if char not in instring:
                instring.append(char)
        for p in range(len(instring)):
            masks.append(vocab[p])
        masked = s
        for i in range(len(instring)):
            masked = masked.replace(instring[i], masks[i])
        result = []
        temp = masked
        for a in instring:
            if a in temp:
                continue
            temp = temp.replace(masks[0], a)
            if len(instring) == 1:
                result.append(temp)
                temp = temp.replace(a, masks[0])
                continue
            for b in instring:
                if b in temp:
                    continue
                temp = temp.replace(masks[1], b)
                if len(instring) == 2:
                    result.append(temp)
                    temp = temp.replace(b, masks[1])
                    continue
                for c in instring:
                    if c in temp:
                        continue
                    temp = temp.replace(masks[2], c)
                    if len(instring) == 3:
                        result.append(temp)
                        temp = temp.replace(c, masks[2])
                        continue
                    for d in instring:
                        if d in temp:
                            continue
                        temp = temp.replace(masks[3], d)
                        if len(instring) == 4:
                            result.append(temp)
                            temp = temp.replace(d, masks[3])
                            continue
                    temp = temp.replace(c, masks[2])
                temp = temp.replace(b, masks[1])
            temp = temp.replace(a, masks[0])
        print(len(result))
        return result

path = "/samples.gpt2/rhymesamples.txt"
test = Test()
test.count(path)
